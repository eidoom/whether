package main

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"os"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"github.com/nathan-osman/go-sunrise"
)

// data as stored in SQLite
type DatabaseTimePointHourly = TimePointHourly[int64]
type DatabaseTimePointThreeHourly = TimePointThreeHourly[int64]

type Info struct {
	PointsHourly      []DatabaseTimePointHourly      `json:"pointsHourly"`
	PointsThreeHourly []DatabaseTimePointThreeHourly `json:"pointsThreeHourly"`
	Location          MyLocation                     `json:"location"`
	Sunrises          []int64                        `json:"sunrises"`
	Sunsets           []int64                        `json:"sunsets"`
}

type MyLocation struct {
	Name      string  `json:"name"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func APItoDB(ctx context.Context, db *sql.DB, apikey string, requestedLatitude float64, requestedLongitude float64) (int64, error) {
	log.Println("API -> DB")

	result, err := callMetOfficeAPI[ApiTimePointHourly](
		"hourly",
		requestedLatitude,
		requestedLongitude,
		apikey,
	)
	if err != nil {
		return 0, err
	}

	name := result.Features[0].Properties.Location.Name
	coords := result.Features[0].Geometry.Coordinates // move under next if when not using it below

	row := db.QueryRowContext(
		ctx,
		"SELECT id FROM location WHERE latitude = ? AND longitude = ?;",
		coords[1],
		coords[0],
	)
	var locationID int64
	err = row.Scan(&locationID)
	if err == sql.ErrNoRows {
		// this is a new location
		log.Println("Adding new location to database")
		res, err := db.ExecContext(
			ctx,
			`
      INSERT INTO location (
          name, latitude, longitude, altitude
      )
      VALUES (
          ?, ?, ?, ?
      );
      `,
			name,
			coords[1],
			coords[0],
			coords[2],
		)
		if err != nil {
			return 0, err
		}
		locationID, err = res.LastInsertId()
		if err != nil {
			return 0, err
		}
	} else {
		// it's a previously used location so use the existing locationID
		if err != nil {
			return 0, err
		}
	}

	log.Println("Adding new hourly forecasts to database")
	for _, v := range result.Features[0].Properties.TimeSeries {
		// insert the forecast point
		// and replace existing points for the same time and location
		// if the new model_run_time is greater
		_, err := db.ExecContext(
			ctx,
			`
            INSERT INTO forecast_hourly (
                added,
                model_run_time,
                location_id,
                time,
                significant_weather_code,
                prob_of_precipitation,
                screen_temperature,
                max_screen_air_temp,
                min_screen_air_temp,
                screen_dew_point_temperature,
                feels_like_temperature,
                wind_direction_from_10m,
                wind_speed_10m,
                wind_gust_speed_10m,
                max_10m_wind_gust,
                visibility,
                screen_relative_humidity,
                uv_index,
                mslp,
                precipitation_rate,
                total_precip_amount,
                total_snow_amount
            )
            VALUES (
                UNIXEPOCH(),
                @model_run_time,
                @location_id,
                @time,
                @significant_weather_code,
                @prob_of_precipitation,
                @screen_temperature,
                @max_screen_air_temp,
                @min_screen_air_temp,
                @screen_dew_point_temperature,
                @feels_like_temperature,
                @wind_direction_from_10m,
                @wind_speed_10m,
                @wind_gust_speed_10m,
                @max_10m_wind_gust,
                @visibility,
                @screen_relative_humidity,
                @uv_index,
                @mslp,
                @precipitation_rate,
                @total_precip_amount,
                @total_snow_amount
            )
            ON CONFLICT (location_id, time) DO UPDATE SET
                added=UNIXEPOCH(),
                model_run_time=@model_run_time,
                time=@time,
                significant_weather_code=@significant_weather_code,
                prob_of_precipitation=@prob_of_precipitation,
                screen_temperature=@screen_temperature,
                max_screen_air_temp=@max_screen_air_temp,
                min_screen_air_temp=@min_screen_air_temp,
                screen_dew_point_temperature=@screen_dew_point_temperature,
                feels_like_temperature=@feels_like_temperature,
                wind_direction_from_10m=@wind_direction_from_10m,
                wind_speed_10m=@wind_speed_10m,
                wind_gust_speed_10m=@wind_gust_speed_10m,
                max_10m_wind_gust=@max_10m_wind_gust,
                visibility=@visibility,
                screen_relative_humidity=@screen_relative_humidity,
                uv_index=@uv_index,
                mslp=@mslp,
                precipitation_rate=@precipitation_rate,
                total_precip_amount=@total_precip_amount,
                total_snow_amount=@total_snow_amount
            WHERE
				@model_run_time > model_run_time
            `,
			sql.Named("model_run_time", result.Features[0].Properties.ModelRunDate.Time.Unix()),
			sql.Named("location_id", locationID),
			sql.Named("time", v.Time.Time.Unix()),
			sql.Named("significant_weather_code", v.SignificantWeatherCode),
			sql.Named("prob_of_precipitation", v.ProbOfPrecipitation),
			sql.Named("screen_temperature", v.ScreenTemperature),
			sql.Named("max_screen_air_temp", v.MaxScreenAirTemp),
			sql.Named("min_screen_air_temp", v.MinScreenAirTemp),
			sql.Named("screen_dew_point_temperature", v.ScreenDewPointTemperature),
			sql.Named("feels_like_temperature", v.FeelsLikeTemperature),
			sql.Named("wind_direction_from_10m", v.WindDirectionFrom10m),
			sql.Named("wind_speed_10m", v.WindSpeed10m),
			sql.Named("wind_gust_speed_10m", v.WindGustSpeed10m),
			sql.Named("max_10m_wind_gust", v.Max10mWindGust),
			sql.Named("visibility", v.Visibility),
			sql.Named("screen_relative_humidity", v.ScreenRelativeHumidity),
			sql.Named("uv_index", v.UvIndex),
			sql.Named("mslp", v.Mslp),
			sql.Named("precipitation_rate", v.PrecipitationRate),
			sql.Named("total_precip_amount", v.TotalPrecipAmount),
			sql.Named("total_snow_amount", v.TotalSnowAmount),
		)
		if err != nil {
			return 0, err
		}
	}

	// TODO delete old forecasts (those from previous days?)

	result3h, err := callMetOfficeAPI[ApiTimePointThreeHourly](
		"three-hourly",
		requestedLatitude,
		requestedLongitude,
		apikey,
	)
	if err != nil {
		return 0, err
	}

	log.Println("Adding new three hourly forecasts to database")
	for _, v := range result3h.Features[0].Properties.TimeSeries {
		// as for hourly
		_, err := db.ExecContext(
			ctx,
			`
            INSERT INTO forecast_three_hourly (
                added,
                model_run_time,
                location_id,
                time,
                max_screen_air_temp,
                min_screen_air_temp,
                max_10m_wind_gust,
                significant_weather_code,
                total_precip_amount,
                total_snow_amount,
                wind_speed_10m,
                wind_direction_from_10m,
                wind_gust_speed_10m,
                visibility,
                mslp,
                screen_relative_humidity,
                feels_like_temp,
                uv_index,
                prob_of_precipitation,
                prob_of_snow,
                prob_of_heavy_snow,
                prob_of_rain,
                prob_of_heavy_rain,
                prob_of_hail,
                prob_of_sferics
            )
            VALUES (
                UNIXEPOCH(),
                @model_run_time,
                @location_id,
                @time,
                @max_screen_air_temp,
                @min_screen_air_temp,
                @max_10m_wind_gust,
                @significant_weather_code,
                @total_precip_amount,
                @total_snow_amount,
                @wind_speed_10m,
                @wind_direction_from_10m,
                @wind_gust_speed_10m,
                @visibility,
                @mslp,
                @screen_relative_humidity,
                @feels_like_temp,
                @uv_index,
                @prob_of_precipitation,
                @prob_of_snow,
                @prob_of_heavy_snow,
                @prob_of_rain,
                @prob_of_heavy_rain,
                @prob_of_hail,
                @prob_of_sferics
            )
            ON CONFLICT (location_id, time) DO UPDATE SET
                added=UNIXEPOCH(),
                model_run_time=@model_run_time,
                time=@time,
                max_screen_air_temp=@max_screen_air_temp,
                min_screen_air_temp=@min_screen_air_temp,
                max_10m_wind_gust=@max_10m_wind_gust,
                significant_weather_code=@significant_weather_code,
                total_precip_amount=@total_precip_amount,
                total_snow_amount=@total_snow_amount,
                wind_speed_10m=@wind_speed_10m,
                wind_direction_from_10m=@wind_direction_from_10m,
                wind_gust_speed_10m=@wind_gust_speed_10m,
                visibility=@visibility,
                mslp=@mslp,
                screen_relative_humidity=@screen_relative_humidity,
                feels_like_temp=@feels_like_temp,
                uv_index=@uv_index,
                prob_of_precipitation=@prob_of_precipitation,
                prob_of_snow=@prob_of_snow,
                prob_of_heavy_snow=@prob_of_heavy_snow,
                prob_of_rain=@prob_of_rain,
                prob_of_heavy_rain=@prob_of_heavy_rain,
                prob_of_hail=@prob_of_hail,
                prob_of_sferics=@prob_of_sferics
            WHERE
				@model_run_time > model_run_time
            `,
			sql.Named("model_run_time", result3h.Features[0].Properties.ModelRunDate.Time.Unix()),
			sql.Named("location_id", locationID),
			sql.Named("time", v.Time.Time.Unix()),
			sql.Named("max_screen_air_temp", v.MaxScreenAirTemp),
			sql.Named("min_screen_air_temp", v.MinScreenAirTemp),
			sql.Named("max_10m_wind_gust", v.Max10mWindGust),
			sql.Named("significant_weather_code", v.SignificantWeatherCode),
			sql.Named("total_precip_amount", v.TotalPrecipAmount),
			sql.Named("total_snow_amount", v.TotalSnowAmount),
			sql.Named("wind_speed_10m", v.WindSpeed10m),
			sql.Named("wind_direction_from_10m", v.WindDirectionFrom10m),
			sql.Named("wind_gust_speed_10m", v.WindGustSpeed10m),
			sql.Named("visibility", v.Visibility),
			sql.Named("mslp", v.Mslp),
			sql.Named("screen_relative_humidity", v.ScreenRelativeHumidity),
			sql.Named("feels_like_temp", v.FeelsLikeTemp),
			sql.Named("uv_index", v.UvIndex),
			sql.Named("prob_of_precipitation", v.ProbOfPrecipitation),
			sql.Named("prob_of_snow", v.ProbOfSnow),
			sql.Named("prob_of_heavy_snow", v.ProbOfHeavySnow),
			sql.Named("prob_of_rain", v.ProbOfRain),
			sql.Named("prob_of_heavy_rain", v.ProbOfHeavyRain),
			sql.Named("prob_of_hail", v.ProbOfHail),
			sql.Named("prob_of_sferics", v.ProbOfSferics),
		)
		if err != nil {
			return 0, err
		}
	}

	log.Println("Success")

	return locationID, nil
}

func DBtoGo(ctx context.Context, db *sql.DB, moment time.Time, locationID int64) (Info, error) {
	log.Println("Querying database for forecasts")

	row := db.QueryRowContext(
		ctx,
		"SELECT name, latitude, longitude FROM location WHERE id = ?;",
		locationID,
	)
	var name string
	var latitude float64
	var longitude float64
	err := row.Scan(&name, &latitude, &longitude)
	if err != nil {
		return Info{}, err
	}

	rows, err := db.QueryContext(
		ctx,
		`
		SELECT
		   time,
		   screen_temperature,
		   max_screen_air_temp,
		   min_screen_air_temp,
		   screen_dew_point_temperature,
		   feels_like_temperature,
		   wind_speed_10m,
		   wind_direction_from_10m,
		   wind_gust_speed_10m,
		   max_10m_wind_gust,
		   visibility,
		   screen_relative_humidity,
		   mslp,
		   uv_index,
		   significant_weather_code,
		   precipitation_rate,
		   total_precip_amount,
		   total_snow_amount,
		   prob_of_precipitation
		FROM forecast_hourly
		WHERE
			location_id = ?
			AND
			time >= ?
		ORDER BY time;
        `,
		locationID,
		moment.Unix(),
	)
	if err != nil {
		return Info{}, err
	}
	defer rows.Close()

	var pointsHourly []DatabaseTimePointHourly

	for rows.Next() {
		var point DatabaseTimePointHourly
		err = rows.Scan(
			&point.Time,
			&point.ScreenTemperature,
			&point.MaxScreenAirTemp,
			&point.MinScreenAirTemp,
			&point.ScreenDewPointTemperature,
			&point.FeelsLikeTemperature,
			&point.WindSpeed10m,
			&point.WindDirectionFrom10m,
			&point.WindGustSpeed10m,
			&point.Max10mWindGust,
			&point.Visibility,
			&point.ScreenRelativeHumidity,
			&point.Mslp,
			&point.UvIndex,
			&point.SignificantWeatherCode,
			&point.PrecipitationRate,
			&point.TotalPrecipAmount,
			&point.TotalSnowAmount,
			&point.ProbOfPrecipitation,
		)
		if err != nil {
			return Info{}, err
		}
		pointsHourly = append(pointsHourly, point)
	}

	firstUnixTime := pointsHourly[0].Time
	lastHourlyUnixTime := time.Unix(pointsHourly[len(pointsHourly)-1].Time, 0).Add(time.Hour).Unix()

	rows3h, err := db.QueryContext(
		ctx,
		`
		SELECT
			time,
			max_screen_air_temp,
			min_screen_air_temp,
			max_10m_wind_gust,
			significant_weather_code,
			total_precip_amount,
			total_snow_amount,
			wind_speed_10m,
			wind_direction_from_10m,
			wind_gust_speed_10m,
			visibility,
			mslp,
			screen_relative_humidity,
			feels_like_temp,
			uv_index,
			prob_of_precipitation,
			prob_of_snow,
			prob_of_heavy_snow,
			prob_of_rain,
			prob_of_heavy_rain,
			prob_of_hail,
			prob_of_sferics
		FROM forecast_three_hourly
		WHERE
			location_id = ?
			AND
			time > ?
		ORDER BY time;
        `,
		locationID,
		lastHourlyUnixTime,
	)
	if err != nil {
		return Info{}, err
	}
	defer rows3h.Close()

	var pointsThreeHourly []DatabaseTimePointThreeHourly

	for rows3h.Next() {
		var point DatabaseTimePointThreeHourly
		err = rows3h.Scan(
			&point.Time,
			&point.MaxScreenAirTemp,
			&point.MinScreenAirTemp,
			&point.Max10mWindGust,
			&point.SignificantWeatherCode,
			&point.TotalPrecipAmount,
			&point.TotalSnowAmount,
			&point.WindSpeed10m,
			&point.WindDirectionFrom10m,
			&point.WindGustSpeed10m,
			&point.Visibility,
			&point.Mslp,
			&point.ScreenRelativeHumidity,
			&point.FeelsLikeTemp,
			&point.UvIndex,
			&point.ProbOfPrecipitation,
			&point.ProbOfSnow,
			&point.ProbOfHeavySnow,
			&point.ProbOfRain,
			&point.ProbOfHeavyRain,
			&point.ProbOfHail,
			&point.ProbOfSferics,
		)
		if err != nil {
			return Info{}, err
		}
		pointsThreeHourly = append(pointsThreeHourly, point)
	}

	lastUnixTime := pointsThreeHourly[len(pointsThreeHourly)-1].Time

	sunrises, sunsets := sunrisesSunsets(latitude, longitude, firstUnixTime, lastUnixTime)

	log.Println("Success")

	return Info{
		PointsHourly:      pointsHourly,
		PointsThreeHourly: pointsThreeHourly,
		Location: MyLocation{
			Name:      name,
			Latitude:  latitude,
			Longitude: longitude,
		},
		Sunrises: sunrises,
		Sunsets:  sunsets,
	}, nil
}

func sunrisesSunsets(
	latitude float64,
	longitude float64,
	firstUnixTime int64,
	lastUnixTime int64,
) ([]int64, []int64) {
	// get earliest datetime and truncate time to give date only
	firstDateTime := time.Unix(firstUnixTime, 0)
	firstDate := time.Date(
		firstDateTime.Year(),
		firstDateTime.Month(),
		firstDateTime.Day(),
		0, 0, 0, 0, time.UTC,
	)

	// get the date after the latest date in the data
	lastDateTime := time.Unix(lastUnixTime, 0)
	lastDate := time.Date(
		lastDateTime.Year(),
		lastDateTime.Month(),
		lastDateTime.Day(),
		0, 0, 0, 0, time.UTC,
	)
	lastDate = lastDate.AddDate(0, 0, 1)

	// loop over the days in the date range and find the sunrise/set times
	var sunrises []int64
	var sunsets []int64
	for d := firstDate; d.Before(lastDate); d = d.AddDate(0, 0, 1) {
		rise, set := sunrise.SunriseSunset(
			latitude, longitude,
			d.Year(), d.Month(), d.Day(),
		)
		if rise.After(firstDateTime) && rise.Before(lastDateTime) {
			sunrises = append(sunrises, rise.Unix())
		}
		if set.After(firstDateTime) && set.Before(lastDateTime) {
			sunsets = append(sunsets, set.Unix())
		}
	}

	return sunrises, sunsets
}

func openDB() (*sql.DB, error) {
	kind := "sqlite3"
	fileName := "cache.db"

	log.Println("Opening", kind, "database connection to", fileName)

	db, err := sql.Open(kind, fileName)
	if err != nil {
		return nil, err
	}

	// apply schema
	schemaFile := "schema.sql"
	schema, err := os.ReadFile(schemaFile)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(string(schema))
	if err != nil {
		return nil, err
	}

	return db, nil
}

func getLocations(ctx context.Context, db *sql.DB) ([]string, error) {
	rows, err := db.QueryContext(ctx, "SELECT name FROM location;")
	if err != nil {
		return []string{}, err
	}
	var locations []string
	for rows.Next() {
		var location string
		err := rows.Scan(&location)
		if err != nil {
			return locations, err
		}
		locations = append(locations, location)
	}
	return locations, nil
}

func getLocation(ctx context.Context, db *sql.DB, name string) (MyLocation, error) {
	row := db.QueryRowContext(ctx, "SELECT name, latitude, longitude FROM location WHERE name = ?;", name)
	var location MyLocation
	err := row.Scan(&location.Name, &location.Latitude, &location.Longitude)
	if err != nil {
		return MyLocation{}, err
	}
	return location, nil
}

func addLocation(ctx context.Context, db *sql.DB, apikey string, latitude float64, longitude float64) (string, error) {
	locationID, err := APItoDB(ctx, db, apikey, latitude, longitude)
	if err != nil {
		return "", err
	}
	row := db.QueryRow("SELECT name FROM location WHERE id = ?;", locationID)
	var location string
	err = row.Scan(&location)
	return location, err
}

var ErrUnknownLocation = errors.New("unknown location")

func getData(ctx context.Context, db *sql.DB, apikey string, location string, moment time.Time) (Info, error) {
	log.Println("Getting data")

	row := db.QueryRowContext(ctx, "SELECT id, latitude, longitude FROM location WHERE name = ?;", location)
	var (
		locationID int64
		latitude   float64
		longitude  float64
	)
	err := row.Scan(&locationID, &latitude, &longitude)
	if err == sql.ErrNoRows {
		return Info{}, ErrUnknownLocation
	} else if err != nil {
		return Info{}, err
	}

	// get time of latest forecast
	// assumes forecast tables (1h, 3h) are always updated (sequentially) simultaneously
	row = db.QueryRowContext(
		ctx,
		`
			SELECT MAX(added) FROM (
				SELECT added, location_id FROM forecast_hourly
				UNION ALL
				SELECT added, location_id FROM forecast_three_hourly
			)
			WHERE location_id = ?;
			`,
		locationID,
	)
	var added sql.NullInt64
	err = row.Scan(&added)
	if err != nil {
		return Info{}, err
	}
	if !added.Valid {
		// a NULL has been returned, meaning no rows in aggregate query
		// => there are no previous forecasts so pull new data
		_, err = APItoDB(ctx, db, apikey, latitude, longitude)
		if err != nil {
			return Info{}, err
		}
	} else {
		// there are previous cached forecasts, so we'll check if they're out of date

		// model_run_time availability: https://datahub.metoffice.gov.uk/support/model-run-availability
		// ^ it's complicated! TODO poll more intelligently

		// we can't call the API on every page load because we have only 360 calls per day
		// https://datahub.metoffice.gov.uk/pricing/site-specific#global-spot
		// so: if more than an hour has passed since last pull, then allow call
		if time.Unix(added.Int64, 0).Add(time.Hour).Before(time.Now()) {
			// cache data may be out of date, pull new forecasts
			_, err = APItoDB(ctx, db, apikey, latitude, longitude)
			if err != nil {
				return Info{}, err
			}
		}
	}

	// use cached forecasts
	info, err := DBtoGo(ctx, db, moment, locationID)
	if err != nil {
		return Info{}, err
	}

	return info, nil
}
