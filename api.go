package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"time"
)

// this is the Met Office site-specific global spot deterministic forecast
// see https://datahub.metoffice.gov.uk/
const BaseURL string = "https://data.hub.api.metoffice.gov.uk/sitespecific/v0/point"

type ApiTimePoint interface {
	ApiTimePointHourly | ApiTimePointThreeHourly
}

type Response[T ApiTimePoint] struct {
	Type     string       `json:"type"`
	Features []Feature[T] `json:"features"`
}

type Feature[T ApiTimePoint] struct {
	Type       string        `json:"type"`
	Geometry   Geometry      `json:"geometry"`
	Properties Properties[T] `json:"properties"`
}

type Properties[T ApiTimePoint] struct {
	Location             Location `json:"location"`
	RequestPointDistance float64  `json:"requestPointDistance"`
	ModelRunDate         ZuluTime `json:"modelRunDate"`
	TimeSeries           []T      `json:"timeSeries"`
}

type Geometry struct {
	Type        string     `json:"type"`
	Coordinates [3]float64 `json:"coordinates"`
}

type Location struct {
	Name string `json:"name"`
}

type ApiTimePointHourly = TimePointHourly[ZuluTime]

type ApiTimePointThreeHourly = TimePointThreeHourly[ZuluTime]

func callMetOfficeAPI[T ApiTimePoint](
	endpoint string,
	latitude float64,
	longitude float64,
	apikey string,
) (Response[T], error) {
	excludeParameterMetadata := true
	includeLocationName := true

	fullURL := fmt.Sprintf(
		"%s/%s?latitude=%f&longitude=%f&excludeParameterMetadata=%t&includeLocationName=%t",
		BaseURL,
		endpoint,
		latitude,
		longitude,
		excludeParameterMetadata,
		includeLocationName,
	)

	// make sure url is valid
	_, err := url.Parse(fullURL)
	if err != nil {
		return Response[T]{}, err
	}

	log.Println("Querying:", fullURL)

	client := &http.Client{Timeout: 10 * time.Second}

	req, err := http.NewRequest("GET", fullURL, nil)
	if err != nil {
		return Response[T]{}, err
	}

	req.Header.Add("apikey", apikey)
	req.Header.Add("accept", "application/json")

	log.Println("Created request")

	resp, err := client.Do(req)
	if err != nil {
		return Response[T]{}, err
	}
	defer resp.Body.Close()

	log.Println("Response status:", resp.Status)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return Response[T]{}, err
	}

	log.Println("Read response")

	var result Response[T]
	err = json.Unmarshal(body, &result)
	if err != nil {
		log.Println("Warning: JSON unmarshal error")
		return Response[T]{}, err
	}

	log.Println("Decoded response as JSON")

	log.Println("Success")

	return result, nil
}
