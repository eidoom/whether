SRC=main.go data.go web.go api.go null.go timepoint.go

.PHONY: dev all clean build

all: build

dev: $(SRC)
	go run $^ -dev

build: main static/d3.v7.min.js
	mkdir -p $@/
	cp $< apikey schema.sql $@/
	mkdir -p $@/templates
	cp $(shell git ls-files templates/) $@/templates/
	mkdir -p $@/static/
	cp $(shell git ls-files static/) $(word 2, $^) $@/static/

main: $(SRC)
	go build $^

static/d3.v7.min.js static/d3.v7.js:
	wget https://d3js.org/$(@F) --directory-prefix $(@D)

clean:
	-rm main
