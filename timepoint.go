package main

import (
	"encoding/json"
	"fmt"
	"time"
)

type UtcTime interface {
	ZuluTime | int64
}

type ZuluTime struct {
	time.Time
}

// TotalShowAmount should be NullUint, but database/sql doesn't have that
type TimePointHourly[T UtcTime] struct {
	// data as received from Met Office hourly
	// max/min/total over previous hour
	Time                      T           `json:"time"`                      // TZ: UTC
	ScreenTemperature         float64     `json:"screenTemperature"`         // cel - air temp
	MaxScreenAirTemp          NullFloat64 `json:"maxScreenAirTemp"`          // cel
	MinScreenAirTemp          NullFloat64 `json:"minScreenAirTemp"`          // cel
	ScreenDewPointTemperature float64     `json:"screenDewPointTemperature"` // cel
	FeelsLikeTemperature      float64     `json:"feelsLikeTemperature"`      // cel
	WindSpeed10m              float64     `json:"windSpeed10m"`              // m/s
	WindDirectionFrom10m      uint        `json:"windDirectionFrom10m"`      // deg
	WindGustSpeed10m          float64     `json:"windGustSpeed10m"`          // m/s
	Max10mWindGust            NullFloat64 `json:"max10mWindGust"`            // m/s
	Visibility                uint        `json:"visibility"`                // m
	ScreenRelativeHumidity    float64     `json:"screenRelativeHumidity"`    // %
	Mslp                      uint        `json:"mslp"`                      // Pa - mean sea level pressure
	UvIndex                   uint        `json:"uvIndex"`                   // dim.less
	SignificantWeatherCode    int         `json:"significantWeatherCode"`    // dim.less
	PrecipitationRate         float64     `json:"precipitationRate"`         // mm/h
	TotalPrecipAmount         NullFloat64 `json:"totalPrecipAmount"`         // mm
	TotalSnowAmount           NullFloat64 `json:"totalSnowAmount"`           // mm
	ProbOfPrecipitation       uint        `json:"probOfPrecipitation"`       // %
}

type TimePointThreeHourly[T UtcTime] struct {
	// data as received from Met Office three-hourly
	// max/min/total over previous three hours
	Time                   T       `json:"time"`                   // TZ: UTC
	MaxScreenAirTemp       float64 `json:"maxScreenAirTemp"`       // cel
	MinScreenAirTemp       float64 `json:"minScreenAirTemp"`       // cel
	Max10mWindGust         float64 `json:"max10mWindGust"`         // m/s
	SignificantWeatherCode int     `json:"significantWeatherCode"` // dim.less
	TotalPrecipAmount      float64 `json:"totalPrecipAmount"`      // mm
	TotalSnowAmount        float64 `json:"totalSnowAmount"`        // mm
	WindSpeed10m           float64 `json:"windSpeed10m"`           // m/s
	WindDirectionFrom10m   uint    `json:"windDirectionFrom10m"`   // deg
	WindGustSpeed10m       float64 `json:"windGustSpeed10m"`       // m/s
	Visibility             uint    `json:"visibility"`             // m
	Mslp                   uint    `json:"mslp"`                   // Pa - mean sea level pressure
	ScreenRelativeHumidity float64 `json:"screenRelativeHumidity"` // %
	FeelsLikeTemp          float64 `json:"feelsLikeTemp"`          // cel // nb different name to TimePointHourly
	UvIndex                uint    `json:"uvIndex"`                // dim.less
	ProbOfPrecipitation    uint    `json:"probOfPrecipitation"`    // %
	ProbOfSnow             uint    `json:"probOfSnow"`             // %
	ProbOfHeavySnow        uint    `json:"probOfHeavySnow"`        // %
	ProbOfRain             uint    `json:"probOfRain"`             // %
	ProbOfHeavyRain        uint    `json:"probOfHeavyRain"`        // %
	ProbOfHail             uint    `json:"probOfHail"`             // %
	ProbOfSferics          uint    `json:"probOfSferics"`          // %
}

func (p *ZuluTime) UnmarshalJSON(bytes []byte) error {
	var raw string
	err := json.Unmarshal(bytes, &raw)

	if err != nil {
		return fmt.Errorf("error decoding ZuluTime field in JSON as string: %w", err)
	}

	// read as UTC
	p.Time, err = time.Parse("2006-01-02T15:04Z", raw)

	if err != nil {
		return fmt.Errorf("error parsing string to ZuluTime: %w", err)
	}

	return nil
}
