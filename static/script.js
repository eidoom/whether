// TODO move tooltip left/right when near horizontal edges
// TODO add d3-zoom

function exists(x) {
  return typeof x !== "undefined";
}

// TODO show significant weather icons
// https://www.metoffice.gov.uk/services/data/datapoint/code-definitions
const weatherCode = new Map([
  [-1, "Trace rain"],
  [0, "Clear night"],
  [1, "Sunny day"],
  [2, "Partly cloudy (night)"],
  [3, "Partly cloudy (day)"],
  [4, "Not used"],
  [5, "Mist"],
  [6, "Fog"],
  [7, "Cloudy"],
  [8, "Overcast"],
  [9, "Light rain shower (night)"],
  [10, "Light rain shower (day)"],
  [11, "Drizzle"],
  [12, "Light rain"],
  [13, "Heavy rain shower (night)"],
  [14, "Heavy rain shower (day)"],
  [15, "Heavy rain"],
  [16, "Sleet shower (night)"],
  [17, "Sleet shower (day)"],
  [18, "Sleet"],
  [19, "Hail shower (night)"],
  [20, "Hail shower (day)"],
  [21, "Hail"],
  [22, "Light snow shower (night)"],
  [23, "Light snow shower (day)"],
  [24, "Light snow"],
  [25, "Heavy snow shower (night)"],
  [26, "Heavy snow shower (day)"],
  [27, "Heavy snow"],
  [28, "Thunder shower (night)"],
  [29, "Thunder shower (day)"],
  [30, "Thunder"],
]);

// function visibilityDesc(visibility) {
//   // unit: metres
//   if (visibility <= 1000) {
//     return "Very poor";
//   } else if (visibility <= 4000) {
//     return "Poor";
//   } else if (visibility <= 10000) {
//     return "Moderate";
//   } else if (visibility <= 20000) {
//     return "Good";
//   } else if (visibility <= 40000) {
//     return "Very good";
//   } else {
//     return "Excellent";
//   }
// }

function visibilityDescShort(visibility) {
  if (visibility <= 1) {
    return "VP";
  } else if (visibility <= 4) {
    return "P";
  } else if (visibility <= 10) {
    return "M";
  } else if (visibility <= 20) {
    return "G";
  } else if (visibility <= 40) {
    return "VG";
  } else {
    return "E";
  }
}

// function uvDesc(uv) {
//   if (uv == 0) {
//     return "None";
//   } else if (uv <= 2) {
//     return "Low";
//   } else if (uv <= 5) {
//     return "Moderate";
//   } else if (uv <= 7) {
//     return "High";
//   } else if (uv <= 10) {
//     return "Very high";
//   } else {
//     return "Extreme";
//   }
// }

function uvDescShort(uv) {
  if (uv == 0) {
    return "-";
  } else if (uv <= 2) {
    return "L";
  } else if (uv <= 5) {
    return "M";
  } else if (uv <= 7) {
    return "H";
  } else if (uv <= 10) {
    return "VH";
  } else {
    return "E";
  }
}

function uvColour(uv) {
  // TODO can I do this with d3-scale-chromatic instead?
  if (uv === 0) {
    return "grey";
  } else if (uv <= 2) {
    return "green";
  } else if (uv <= 5) {
    return "yellow";
  } else if (uv <= 7) {
    return "orange";
  } else if (uv <= 10) {
    return "red";
  } else {
    return "purple";
  }
}

function compass(deg) {
  // ensure deg from 0 to 359 inclusive
  deg = (deg + 360) % 360;

  if (deg >= 349 || deg <= 11) {
    // -11.25 (348.75) -- 11.25
    return "N";
  } else if (deg > 11 && deg < 34) {
    // 11.25 -- 33.75
    return "NNE";
  } else if (deg >= 34 && deg <= 56) {
    // 33.75 -- 56.25
    return "NE";
  } else if (deg > 56 && deg < 79) {
    // 56.25 -- 78.75
    return "ENE";
  } else if (deg >= 79 && deg <= 101) {
    // 78.75 -- 101.25
    return "E";
  } else if (deg > 101 && deg < 124) {
    // 101.25 -- 123.75
    return "ESE";
  } else if (deg >= 124 && deg <= 146) {
    // 123.75 -- 146.25
    return "SE";
  } else if (deg > 146 && deg < 169) {
    // 146.25 -- 168.75
    return "SSE";
  } else if (deg >= 169 && deg <= 191) {
    // 168.75 -- 191.25
    return "S";
  } else if (deg > 191 && deg < 214) {
    // 191.25 -- 213.75
    return "SSW";
  } else if (deg >= 214 && deg <= 236) {
    // 213.75 -- 236.25
    return "SW";
  } else if (deg > 236 && deg < 259) {
    // 236.25 -- 258.75
    return "WSW";
  } else if (deg >= 258 && deg <= 281) {
    // 258.75 -- 281.25
    return "W";
  } else if (deg > 281 && deg < 304) {
    // 281.25 -- 303.75
    return "WNW";
  } else if (deg >= 304 && deg <= 326) {
    // 303.75 -- 326.25
    return "NW";
  } else if (deg > 326 && deg < 349) {
    // 326.25 -- 348.75
    return "NNW";
  } else {
    return "";
  }
}

const compose = (f1, f2) => (a) => f2(f1(a));

const stripZero = (x) => x.replace(/^0/, "");

const dayF = d3.timeFormat("%a");
const hourF = compose(d3.timeFormat("%I"), stripZero);
const meridiemF = d3.timeFormat("%p");
const hourMeridiemF = compose(d3.timeFormat("%I %p"), stripZero);

const series = [
  ["probOfPrecipitation", "Precip. Prob.", "%"],
  ["screenTemperature", "Temperature", "°C"],
  ["feelsLikeTemperature", "Feels like", "°C"],
  ["windDirectionFrom10m", "Wind Direction", "°"],
  ["windSpeed10m", "Wind Speed", "km/h"],
  ["windGustSpeed10m", "Gust Speed", "km/h"],
  ["visibility", "Visibility", "km"],
  ["screenRelativeHumidity", "Rel. Humidity", "%"],
  ["uvIndex", "UV index"],
  ["mslp", "MSL pressure", "kPa"],
  // ["precipitationRate", "Precip. rate", "mm/h"],
   ["totalPrecipAmount", "Tot. precip.", "mm"],
   ["totalSnowAmount", "Tot. snow", "mm"],
  // ["probOfSnow", "Snow Prob.", "%"], // only for 3h so would need to get 3h data in 1h period too
];

const container = d3.select("#d3");

const widthOuter = 2000,
  subheight = 100,
  nSeries = series.length,
  marginInner = 20,
  leftPad = 40, // between y-axis and start of plotted data
  labelPad = 50, // between y-axis and its label
  marginTop = 20,
  marginRight = 40,
  marginBottom = 20,
  marginLeft = 60,
  heightOuter =
    nSeries * subheight +
    (nSeries + 1) * marginInner +
    marginTop +
    marginBottom;

/* nSeries = 2
 *  +-+------+-+
 *  | |      | | marginTop
 *  +-+------+-+
 *  | |      | | marginInner
 *  +-+------+-+ top x axis here
 *  | |      | | subheight
 *  | |      | | subplot 0 here
 *  +-+------+-+
 *  | |      | | marginInner
 *  +-+------+-+
 *  | |      | | subheight
 *  | |      | | subplot 1 here
 *  +-+------+-+
 *  | |      | | marginInner
 *  +-+------+-+ bottom x axis here
 *  | |      | | marginBottom
 *  +-+------+-+
 */

const svg = container
  .append("svg")
  .attr("width", widthOuter)
  .attr("height", heightOuter)
  .attr("viewBox", [0, 0, widthOuter, heightOuter])
  .classed("plot", true);

d3.json("./data.json").then((data) => {
  data.sunrises = data.sunrises.map((x) => new Date(1000 * x));
  data.sunsets = data.sunsets.map((x) => new Date(1000 * x));

  for (const x of data.pointsThreeHourly) {
    // for some reason the Met Office folk decided to change this field name
    // between the hourly and three-hourly endpoints
    x.feelsLikeTemperature = x.feelsLikeTemp;
    delete x.feelsLikeTemp;

    // each point is an average over the preceeding 3 hours
    // period is in units of minutes
    x.period = 180;
  }

  data.pointsHourly = data.pointsHourly.map((x) => ({ ...x, period: 60 }));

  const points = [...data.pointsHourly, ...data.pointsThreeHourly];

  let last = points[0].windDirectionFrom10m;
  for (const x of points) {
    x.endTime = new Date(1000 * x.time); // unixepoch -> Date
    x.midTime = d3.timeMinute.offset(x.endTime, -x.period / 2);
    x.startTime = d3.timeMinute.offset(x.endTime, -x.period);

    x.visibility /= 1000; // m -> km

    // this is to stop the value wrapping round the 360° boundary so the line doesn't break
    if (x.windDirectionFrom10m < last - 180) {
      x.windDirectionFrom10m += 360;
    } else if (x.windDirectionFrom10m > last + 180) {
      x.windDirectionFrom10m -= 360;
    }
    last = x.windDirectionFrom10m;

    // m/s -> km/h
    x.windSpeed10m *= 3.6;
    x.windGustSpeed10m *= 3.6;
    if (exists(x.max10mWindGust)) {
      x.max10mWindGust *= 3.6;
    }

    // Pa -> kPa
    x.mslp = x.mslp / 1000;
  }

  // x scale
  const xScale = d3
    .scaleTime()
    .domain(
      d3.extent([
        ...points.map((x) => x.startTime),
        ...points.map((x) => x.endTime),
      ]),
    )
    .range([marginLeft + leftPad, widthOuter - marginRight]);

  // day/night shading
  const daytimes = svg.append("g");

  const earliest = points[0].startTime;
  const latest = points[points.length - 1].endTime;

  for (
    let i = 0;
    i <= Math.min(data.sunrises.length, data.sunsets.length);
    i++
  ) {
    const x0 = xScale(
        (earliest > data.sunrises[0]
          ? null
          : data.sunrises[data.sunrises[0] < data.sunsets[0] ? i : i - 1]) ??
          earliest,
      ),
      x1 = xScale(data.sunsets[i] ?? latest);

    const path = d3.path();
    path.rect(
      x0,
      marginTop,
      x1 - x0,
      marginTop + nSeries * (subheight + marginInner),
    );

    daytimes
      .append("path")
      .attr("stroke", "none")
      .attr("fill", d3.color("hsla(28, 85%, 58%, 0.1)"))
      .attr("d", path);
  }

  // x axis bottom
  // ticks could depend on width
  svg
    .append("g")
    .attr("transform", `translate(0,${heightOuter - marginBottom})`)
    .call(d3.axisBottom(xScale));

  // x axis top
  svg
    .append("g")
    .attr("transform", `translate(0,${marginTop})`)
    .call(d3.axisTop(xScale));

  // data series
  series.forEach(([field, label, unit], i) => {
    const top = marginTop + i * subheight + (i + 1) * marginInner;

    const subplot = svg.append("g").attr("transform", `translate(0,${top})`);

    // y scale
    let yScale;
    if (field === "screenTemperature") {
      yScale = d3
        .scaleLinear()
        .domain(
          d3.extent([
            ...points.map((x) => x.minScreenAirTemp),
            ...points.map((x) => x.screenTemperature),
            ...points.map((x) => x.maxScreenAirTemp),
          ]),
        )
        .range([subheight, 0])
        .nice();
    } else if (field === "windGustSpeed10m") {
      yScale = d3
        .scaleLinear()
        .domain(
          d3.extent([
            ...points.map((x) => x.windGustSpeed10m),
            ...points.map((x) => x.max10mWindGust),
          ]),
        )
        .range([subheight, 0])
        .nice();
    } else {
      yScale = d3
        .scaleLinear()
        .domain(d3.extent(points, (d) => d[field]))
        .range([subheight, 0])
        .nice();
    }

    // y axis
    const unitText = exists(unit) ? ` (${unit})` : "";
    subplot
      .append("g")
      .attr("transform", `translate(${marginLeft},0)`)
      // TODO ticks could depend on range and height
      .call(d3.axisLeft(yScale).ticks(5))
      .call((g) =>
        g
          .append("text")
          .attr("fill", "currentColor")
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "base")
          .attr("transform", "rotate(-90)")
          .attr("x", -subheight / 2)
          .attr("y", -labelPad)
          .text(label + unitText),
      );

    // // x error
    // function hError(data) {
    //   // const
    //   const w = 3;
    //   const y = yScale(data[field]),
    //     x0 = xScale(data.startTime),
    //     x1 = xScale(data.endTime);
    //   const path = d3.path();
    //   // Horizonal line
    //   path.moveTo(x0, y);
    //   path.lineTo(x1, y);

    //   // Left error bar
    //   path.moveTo(x0, y - w);
    //   path.lineTo(x0, y + w);

    //   // Right error bar
    //   path.moveTo(x1, y - w);
    //   path.lineTo(x1, y + w);

    //   return path;
    // }

    // subplot
    //   .append("g")
    //   .selectAll("path")
    //   .data(points)
    //   .enter()
    //   .append("path")
    //   .attr("d", hError)
    //   .attr("fill", "none")
    //   .attr("stroke-width", 1)
    //   .attr("stroke", "currentColor");

    // vertical errors
    if (field === "screenTemperature") {
      const area = d3
        .area()
        .x((d) => xScale(d.midTime))
        .y0((d) => yScale(d.minScreenAirTemp))
        .y1((d) => yScale(d.maxScreenAirTemp))
        .defined(
          (d) => exists(d.minScreenAirTemp) && exists(d.maxScreenAirTemp),
        )
        .curve(d3.curveCatmullRom.alpha(0.5));

      subplot
        .append("g")
        .append("path")
        .attr("stroke", "none")
        .attr("fill", "blue")
        .attr("opacity", 0.2)
        .attr("d", area(points));

      function errorBar(data) {
        // const
        const w = 3;
        const x = xScale(data.midTime),
          y0 = yScale(data.minScreenAirTemp),
          y1 = yScale(data.maxScreenAirTemp);
        const path = d3.path();
        // Vertical line
        path.moveTo(x, y0);
        path.lineTo(x, y1);
        // Bottom error bar
        path.moveTo(x - w, y0);
        path.lineTo(x + w, y0);
        // Top error bar
        path.moveTo(x - w, y1);
        path.lineTo(x + w, y1);
        return path;
      }

      subplot
        .append("g")
        .selectAll("path")
        .data(
          points.filter(
            (d) => exists(d.minScreenAirTemp) && exists(d.maxScreenAirTemp),
          ),
        )
        .enter()
        .append("path")
        .attr("d", errorBar)
        .attr("fill", "none")
        .attr("stroke-width", 1)
        .attr("stroke", "currentColor");
    } else if (field === "windGustSpeed10m") {
      const area = d3
        .area()
        .x((d) => xScale(d.midTime))
        .y0((d) => yScale(d.windGustSpeed10m))
        .y1((d) => yScale(d.max10mWindGust))
        .defined((d) => exists(d.max10mWindGust))
        .curve(d3.curveCatmullRom.alpha(0.5));

      subplot
        .append("g")
        .append("path")
        .attr("stroke", "none")
        .attr("fill", "blue")
        .attr("opacity", 0.2)
        .attr("d", area(points));

      function errorBar(data) {
        // const
        const w = 3;
        const x = xScale(data.midTime),
          y = yScale(data.windGustSpeed10m),
          y1 = yScale(data.max10mWindGust);
        const path = d3.path();
        // Vertical line
        path.moveTo(x, y);
        path.lineTo(x, y1);
        // Top error bar
        path.moveTo(x - w, y1);
        path.lineTo(x + w, y1);
        return path;
      }

      subplot
        .append("g")
        .selectAll("path")
        .data(points.filter((d) => exists(d.max10mWindGust)))
        .enter()
        .append("path")
        .attr("d", errorBar)
        .attr("fill", "none")
        .attr("stroke-width", 1)
        .attr("stroke", "currentColor");
    }

    // fit and draw line
    if (field === "screenTemperature") {
      // for 3h, have only max and min
      const line = d3
        .line()
        .x((d) => xScale(d.midTime))
        .y((d) =>
          yScale(
            exists(d.screenTemperature)
              ? d.screenTemperature
              : (d.minScreenAirTemp + d.maxScreenAirTemp) / 2,
          ),
        )
        .curve(d3.curveCatmullRom.alpha(0.5));

      subplot
        .append("g")
        .append("path")
        .attr("fill", "none")
        .attr("stroke", "currentColor")
        .attr("stroke-width", 1)
        .attr("d", line(points));
    } else {
      const line = d3
        .line()
        .x((d) => xScale(d.midTime))
        .y((d) => yScale(d[field]))
        .curve(d3.curveCatmullRom.alpha(0.5));

      subplot
        .append("g")
        .append("path")
        .attr("fill", "none")
        .attr("stroke", "currentColor")
        .attr("stroke-width", 1)
        .attr("d", line(points.filter((d) => exists(d[field]))));
    }

    // plot data points
    subplot
      .append("g")
      .selectAll("circle")
      .data(points.filter((d) => exists(d[field])))
      .join("circle")
      .attr("r", 3)
      .attr("fill", (d) => {
        // TODO could distinguish in a more structured fashion using class inheritance
        if (field === "uvIndex") {
          return uvColour(d[field]);
        }
        return "currentColor";
      })
      .attr("cx", (d) => xScale(d.midTime))
      .attr("cy", (d) => yScale(d[field]));
  });

  // tooltip
  const dot = svg.append("g").attr("display", "none");

  const tt = dot
    .append("text")
    .attr("text-anchor", "middle")
    .attr("dominant-baseline", "bottom")
    .attr("fill", "currentColor");

  svg
    .on("pointerenter", pointerentered)
    .on("pointermove", pointermoved)
    .on("pointerleave", pointerleft)
    // this means a touch drag event can interact, but disables scrolling
    .on("touchstart", (event) => event.preventDefault());

  const thumbMargin = 70;
  const timepoints = points.map((d) => xScale(d.midTime));
  // wrt yy (in pointermoved) not y
  const subplotMids = series.map(
    (_, i) => (i + 0.5) * subheight + i * marginInner,
  );

  function pointermoved(event) {
    const [xm, y] = d3.pointer(event);
    const yy = y - marginTop - marginInner;

    const i = d3.leastIndex(timepoints, (x) => Math.abs(x - xm));
    const j = d3.leastIndex(subplotMids, (y) => Math.abs(y - yy));

    const x = timepoints[i];

    const gap = (yy < thumbMargin ? 1 : -1) * thumbMargin;

    let [name, _, unit] = series[j];

    let value = points[i][name];

    let text;
    if (name === "screenTemperature") {
      let min = points[i].minScreenAirTemp;
      let max = points[i].maxScreenAirTemp;

      if (exists(value)) {
        if (exists(min) && exists(max)) {
          text = `${value.toFixed(1)} (${min.toFixed(1)}–${max.toFixed(1)}) ${unit}`;
        } else {
          text = `${value.toFixed(1)} ${unit}`;
        }
      } else {
        if (exists(min) && exists(max)) {
          text = `${min.toFixed(1)}–${max.toFixed(1)} ${unit}`;
        }
      }
    } else if (name === "windGustSpeed10m") {
      let max = points[i].max10mWindGust;
      if (exists(max)) {
        text = `${value.toFixed(1)} (<${max.toFixed(1)}) ${unit}`;
      } else {
        text = `${value.toFixed(1)} ${unit}`;
      }
    } else {
      if (name === "windDirectionFrom10m") {
        value = (value + 360) % 360;
      }

      // if value has nonzero decimal places, round to 1 decimal place
      value = value % 1 === 0 ? value : value.toFixed(1);

      // print empty string for dimensionless values
      unit = typeof unit !== "undefined" ? unit : "";
      // only add a gap between value and unit if not one of the specified units
      unit = "°%".includes(unit[0]) ? unit : ` ${unit}`;

      text = value + unit;

      switch (name) {
        case "uvIndex":
          text = `${text} (${uvDescShort(value)})`;
          break;
        case "visibility":
          text = `${text} (${visibilityDescShort(value)})`;
          break;
        case "windDirectionFrom10m":
          text = `${text} (${compass(value)})`;
          break;
      }
    }

    dot.attr("transform", `translate(${x},${y + gap})`);

    const endTime = points[i].endTime,
      startTime = points[i].startTime,
      endDay = dayF(endTime),
      startDay = dayF(startTime);

    const sTime =
      startDay +
      " " +
      (meridiemF(startTime) === meridiemF(endTime)
        ? hourF(startTime)
        : hourMeridiemF(startTime)) +
      "–" +
      (startDay === endDay ? "" : endDay + " ") +
      hourMeridiemF(endTime);

    tt.text("");
    tt.append("tspan").text(sTime);
    tt.append("tspan").text(text).attr("x", 0).attr("dy", 20);
  }

  function pointerentered() {
    dot.attr("display", null);
  }

  function pointerleft() {
    dot.attr("display", "none");
  }
});
