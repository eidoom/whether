package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"reflect"
)

// next 2: so we can define UnmarshalJSON on them
type NullFloat64 struct {
	sql.NullFloat64
}

type NullInt64 struct {
	sql.NullInt64
}

func (p *NullFloat64) UnmarshalJSON(bytes []byte) error {
	var raw float64
	err := json.Unmarshal(bytes, &raw)

	if err != nil {
		return err
	}

	p.Float64 = raw
	p.Valid = true

	return nil
}

func (p *NullInt64) UnmarshalJSON(bytes []byte) error {
	var raw int64
	err := json.Unmarshal(bytes, &raw)

	if err != nil {
		return err
	}

	p.Int64 = raw
	p.Valid = true

	return nil
}

// to handle omitting null fields
// this only works with T = int64 because ZuluTime is not handled (and not needed)
func (p *TimePointHourly[T]) MarshalJSON() ([]byte, error) {
	var buf bytes.Buffer
	buf.WriteString("{")

	vs := reflect.ValueOf(*p)
	nn := vs.NumField()
	for i := 0; i < nn; i++ {
		comma := true
		n := vs.Type().Field(i).Tag.Get("json")
		vv := vs.Field(i)
		v := vv.Interface()

		switch v.(type) {
		case NullFloat64:
			if vv.FieldByName("Valid").Bool() {
				buf.WriteString(fmt.Sprintf(`"%s":`, n))
				d, err := json.Marshal(vv.FieldByName("Float64").Float())
				if err != nil {
					return nil, err
				}
				buf.Write(d)
			} else {
				comma = false
			}
		case NullInt64:
			if vv.FieldByName("Valid").Bool() {
				buf.WriteString(fmt.Sprintf(`"%s":`, n))
				d, err := json.Marshal(vv.FieldByName("Int64").Int())
				if err != nil {
					return nil, err
				}
				buf.Write(d)
			} else {
				comma = false
			}
		default:
			buf.WriteString(fmt.Sprintf(`"%s":`, n))
			d, err := json.Marshal(v)
			if err != nil {
				return nil, err
			}
			buf.Write(d)
		}

		if comma {
			buf.WriteString(",")
		}
	}

	// remove the trailing comma (due to appending after every entry, not missing fields)
	buf.Truncate(buf.Len() - 1)

	buf.WriteString("}")
	return buf.Bytes(), nil
}
