CREATE TABLE IF NOT EXISTS location (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL UNIQUE,
	latitude REAL NOT NULL,
	longitude REAL NOT NULL,
	altitude REAL NOT NULL
);

-- the nullable fields are because sometimes the
-- last one or two time points don't include them
CREATE TABLE IF NOT EXISTS forecast_hourly (
	id INTEGER PRIMARY KEY,
	added INTEGER NOT NULL,
	model_run_time INTEGER NOT NULL,
	location_id INTEGER NOT NULL REFERENCES location (id),
	time INTEGER NOT NULL,
	significant_weather_code INTEGER NOT NULL,
	prob_of_precipitation INTEGER NOT NULL,
	screen_temperature REAL NOT NULL,
	max_screen_air_temp REAL,
	min_screen_air_temp REAL,
	screen_dew_point_temperature REAL NOT NULL,
	feels_like_temperature REAL NOT NULL,
	wind_direction_from_10m INTEGER NOT NULL,
	wind_speed_10m REAL NOT NULL,
	wind_gust_speed_10m REAL NOT NULL,
	max_10m_wind_gust REAL,
	visibility INTEGER NOT NULL,
	screen_relative_humidity REAL NOT NULL,
	uv_index INTEGER NOT NULL,
	mslp INTEGER NOT NULL,
	precipitation_rate REAL NOT NULL,
	total_precip_amount REAL,
	total_snow_amount INTEGER,
	UNIQUE (location_id, time)
);

CREATE TABLE IF NOT EXISTS forecast_three_hourly (
	-- over previous three hours
	id INTEGER PRIMARY KEY,
	added INTEGER NOT NULL, -- unixepoch UTC
	model_run_time INTEGER NOT NULL, -- unixepoch UTC
	location_id INTEGER NOT NULL REFERENCES location (id),
	time INTEGER NOT NULL, -- unixepoch UTC
	max_screen_air_temp REAL NOT NULL, -- °C
	min_screen_air_temp REAL NOT NULL, -- °C
	max_10m_wind_gust REAL NOT NULL, -- m/s
	significant_weather_code INTEGER NOT NULL,
	total_precip_amount REAL NOT NULL, -- mm
	total_snow_amount INTEGER NOT NULL, -- mm
	wind_speed_10m REAL NOT NULL, -- m/s
	wind_direction_from_10m INTEGER NOT NULL, -- °
	wind_gust_speed_10m REAL NOT NULL, -- m/s
	visibility INTEGER NOT NULL, -- m
	mslp INTEGER NOT NULL, -- Pa
	screen_relative_humidity REAL NOT NULL, -- %
	feels_like_temp REAL NOT NULL, -- °C
	uv_index INTEGER NOT NULL,
	prob_of_precipitation INTEGER NOT NULL, -- %
	prob_of_snow INTEGER NOT NULL, -- %
	prob_of_heavy_snow INTEGER NOT NULL, -- %
	prob_of_rain INTEGER NOT NULL, -- %
	prob_of_heavy_rain INTEGER NOT NULL, -- %
	prob_of_hail INTEGER NOT NULL, -- %
	prob_of_sferics INTEGER NOT NULL, -- %
	UNIQUE (location_id, time)
);
