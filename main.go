package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"html/template"
	"io"
	"log"
	"math"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

func getBaseTemplate(tmplDir string) (*template.Template, error) {
	tmplBaseFile := path.Join(tmplDir, "base.html.tmpl")
	log.Println("Reading template:", tmplBaseFile)
	return template.ParseFiles(tmplBaseFile)
}

func getIndexTemplate(tmplDir string, tmplBase *template.Template) (*template.Template, error) {
	tmplIndexFile := path.Join(tmplDir, "index.html.tmpl")
	log.Println("Reading template:", tmplIndexFile)
	init, err := tmplBase.Clone()
	if err != nil {
		return nil, err
	}
	return init.ParseFiles(tmplIndexFile)
}

func getForecastTemplate(tmplDir string, tmplBase *template.Template) (*template.Template, error) {
	tmplFile := path.Join(tmplDir, "forecast.html.tmpl")
	log.Println("Reading template:", tmplFile)
	init, err := tmplBase.Clone()
	if err != nil {
		return nil, err
	}
	return init.
		Funcs(template.FuncMap{
			"round": math.Round,
		}).
		ParseFiles(tmplFile)
}

type Params struct {
	Location  MyLocation
	ISOformat string
	D3Min     string
}

func badRequest(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusBadRequest)
	w.Header().Set("Content-Type", "text/plain")
	io.WriteString(w, err.Error())
}

func unauthorised(w http.ResponseWriter, mesg string) {
	w.WriteHeader(http.StatusUnauthorized)
	w.Header().Set("Content-Type", "text/plain")
	io.WriteString(w, mesg)
}

func indexHandler(db *sql.DB, tmpl *template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		locations, err := getLocations(r.Context(), db)
		if err != nil {
			badRequest(w, err)
			return
		}
		tmpl.Execute(w, locations)
	}
}

func addLocationHandler(db *sql.DB, token string, apikey string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()

		respToken := r.Form.Get("token")

		if respToken != token {
			unauthorised(w, "wrong token")
			return
		}

		latitude, err := strconv.ParseFloat(r.Form.Get("latitude"), 64)
		if err != nil {
			badRequest(w, err)
			return
		}

		longitude, err := strconv.ParseFloat(r.Form.Get("longitude"), 64)
		if err != nil {
			badRequest(w, err)
			return
		}

		location, err := addLocation(
			r.Context(),
			db,
			apikey,
			latitude,
			longitude,
		)
		if err != nil {
			badRequest(w, err)
			return
		}

		http.Redirect(w, r, "/forecast/"+location+"/", http.StatusSeeOther)
	}
}

func partialForecastHandler(db *sql.DB, tmpl *template.Template, data Params) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		locName := r.PathValue("location")
		locData, err := getLocation(r.Context(), db, locName)
		if err != nil {
			if err == sql.ErrNoRows {
				http.NotFound(w, r)
			} else {
				badRequest(w, err)
			}
			return
		}
		data.Location = locData
		tmpl.Execute(w, data)
	}
}

func partialForecastHandlerDev(db *sql.DB, data Params, tmplDir string, tmplBase *template.Template) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		locName := r.PathValue("location")
		locData, err := getLocation(r.Context(), db, locName)
		if err != nil {
			if err == sql.ErrNoRows {
				http.NotFound(w, r)
			} else {
				badRequest(w, err)
			}
			return
		}
		data.Location = locData
		tmpl, err := getForecastTemplate(tmplDir, tmplBase)
		if err != nil {
			badRequest(w, err)
			return
		}
		tmpl.Execute(w, data)
	}
}

func check(w http.ResponseWriter, err error) bool {
	if err != nil {
		log.Println(err.Error())
		badRequest(w, err)
		return true
	}
	return false
}

func dataHandler(db *sql.DB, apikey string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		// TODO pass as URL param
		moment := time.Now()

		data, err := getData(r.Context(), db, apikey, r.PathValue("location"), moment)
		if check(w, err) {
			return
		}

		b, err := json.Marshal(data)
		if check(w, err) {
			return
		}

		_, err = w.Write(b)
		if check(w, err) {
			return
		}
	}
}

func getKey(filename string) (string, error) {
	keyRaw, err := os.ReadFile(filename)
	if err != nil {
		return "", err
	}
	return strings.TrimSuffix(string(keyRaw), "\n"), nil
}

func main() {
	port := getPort()

	devPtr := flag.Bool("dev", false, "reparse template on page reload")
	flag.Parse()

	var d3min string
	if *devPtr {
		d3min = ""
	} else {
		d3min = ".min"
	}

	apikey, err := getKey("apikey")
	if err != nil {
		log.Fatal(err)
	}

	token, err := getKey("token")
	if err != nil {
		log.Fatal(err)
	}

	db, err := openDB()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	tmplDir := "templates"

	tmplBase, err := getBaseTemplate(tmplDir)
	if err != nil {
		log.Fatal(err)
	}

	tmplIndex, err := getIndexTemplate(tmplDir, tmplBase)
	if err != nil {
		log.Fatal(err)
	}

	mux := http.NewServeMux()

	mux.Handle("/{$}", chain(indexHandler(db, tmplIndex), logging()))

	mux.Handle("/add_location/", chain(addLocationHandler(db, token, apikey), logging()))

	fs := http.FileServer(http.Dir("static/"))
	mux.Handle("/static/", http.StripPrefix("/static/", fs))

	params := Params{
		ISOformat: time.RFC3339Nano,
		Location:  MyLocation{},
		D3Min:     d3min,
	}

	var forecastHandler http.HandlerFunc
	if *devPtr {
		log.Println("Running in dev mode")
		forecastHandler = partialForecastHandlerDev(db, params, tmplDir, tmplBase)
	} else {
		tmplForecast, err := getForecastTemplate(tmplDir, tmplBase)
		if err != nil {
			log.Fatal(err)
		}
		forecastHandler = partialForecastHandler(db, tmplForecast, params)
	}

	mux.HandleFunc("/forecast/{location}/", chain(forecastHandler, logging()))
	mux.HandleFunc("/forecast/{location}/data.json", chain(dataHandler(db, apikey), logging()))

	server := &http.Server{
		Addr:    ":" + port,
		Handler: mux,
	}

	log.Println("Listening...")
	log.Fatal(server.ListenAndServe())
}
