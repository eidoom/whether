# whether

## dev

```sh
make static/d3.v7.js
make dev
```

and open <http://localhost:3000/>.

## deploy

### local

```sh
./deploy.sh <remote>
```

### remote

#### test

```sh
cd ~/deploy/whether
PORT=3002 ./main
```

and open `http://<remote hostname>:3002/`.

#### nginx proxy

After setting CNAME record on nameserver,

```sh
sudo vim /etc/nginx/sites-available/whether.conf
```

```nginx
server {
    listen 80;

    server_name whether.DOMAIN;

    location / {
        proxy_pass http://127.0.0.1:3002/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

```sh
sudo systemctl restart nginx
```

To access at `http://whether.DOMAIN/` once server started.
A simple setup without HTTPS for running on LAN.

#### systemd service

```sh
sudo vim /etc/systemd/system/whether.service
```

```systemd
[Unit]
Description=Whether
After=network.target

[Service]
User=USER
Group=USER

WorkingDirectory=/home/USER/deploy/whether
Environment="PORT=3002"
ExecStart=/home/USER/deploy/whether/main

Type=simple
TimeoutStopSec=20
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

```sh
sudo systemctl enable whether
sudo sysmtectl start whether
```
