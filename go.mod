module whether

go 1.22.2

require (
	github.com/mattn/go-sqlite3 v1.14.22 // indirect
	github.com/nathan-osman/go-sunrise v1.1.0 // indirect
)
